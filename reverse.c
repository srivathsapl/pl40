#include<stdio.h>


int input()
{
    int num;
    printf("Enter a number\n");
    scanf("%d",&num);
    return num;
}
int compute(int num)
{
    int rev=0,rem;
    while(num>0)
    {
        rem=num%10;
        rev=rev*10+rem;
        num=num/10;
    }
    return rev;
}

void output(int num,int rev)
{
   printf("the reversed number is %d\n",rev);
   if(num==rev)
    {
        printf("it is a palindrome");
    }
    else
    {
        printf("not a palindrome");
    }
}

int main()
{
    int num,rev;
    num=input();
    rev=compute(num);
    output(num,rev);
    return 0;
}